//+build unit

package service

import (
	"bytes"
	"net/http/httptest"

	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ramazan.apa/add-todo-app-go/model"

	"encoding/json"
	"net/http"
)

func TestGetAddTodos(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(GetTodos)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected, _ := json.Marshal(model.AddTodos{})
	expectedJsonString := string(expected)
	assert.Equal(t, expectedJsonString, rr.Body.String())
}

func TestAddTodos(t *testing.T) {
	itemTobeAdded := model.AddTodo{
		Text: "this todo should be added properly",
	}
	clearRepo()
	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(itemTobeAdded)
	req, err := http.NewRequest("POST", "/", payloadBuf)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(AddTodo)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response
	expected, _ := json.Marshal(Return{
		model.AddTodo{
			Text: "this todo should be added properly",
		},
	})
	expectedJsonString := string(expected)
	assert.Equal(t, expectedJsonString, rr.Body.String())
}

func clearRepo() {
	TodoRepo.OurTodos = model.AddTodos{}
}
