FROM golang:1.17-alpine as build

RUN mkdir /app
ADD . /app
WORKDIR /app

# Copy the Go App to the container
COPY . .

# Download all dependencies
RUN go get -d -v ./...


#Install the package
RUN go install -v ./...

RUN go build -o main .

#EXPOSE 8000

CMD ["/app/main"]

