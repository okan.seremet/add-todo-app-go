package main

import (
	"github.com/gorilla/handlers"
	"gitlab.com/ramazan.apa/add-todo-app-go/handler"
	"log"
	"net/http"
)

func main() {
	hh := handler.GetHTTPHandler()

	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})
	log.Printf("API starting: port %d", 8080)
	log.Fatal(http.ListenAndServe(":8080", handlers.CORS(originsOk, headersOk, methodsOk)(hh)))

}
